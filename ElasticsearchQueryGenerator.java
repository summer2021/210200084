/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.facebook.presto.elasticsearch;

import com.facebook.airlift.log.Logger;
import com.facebook.presto.common.type.BigintType;
import com.facebook.presto.common.type.TypeManager;
import com.facebook.presto.elasticsearch.ElasticsearchQueryGeneratorContext.Selection;
import com.facebook.presto.spi.ConnectorSession;
import com.facebook.presto.spi.PrestoException;
import com.facebook.presto.spi.function.FunctionMetadataManager;
import com.facebook.presto.spi.function.StandardFunctionResolution;
import com.facebook.presto.spi.plan.AggregationNode;
import com.facebook.presto.spi.plan.FilterNode;
import com.facebook.presto.spi.plan.LimitNode;
import com.facebook.presto.spi.plan.MarkDistinctNode;
import com.facebook.presto.spi.plan.PlanNode;
import com.facebook.presto.spi.plan.PlanVisitor;
import com.facebook.presto.spi.plan.ProjectNode;
import com.facebook.presto.spi.plan.TableScanNode;
import com.facebook.presto.spi.relation.CallExpression;
import com.facebook.presto.spi.relation.RowExpression;
import com.facebook.presto.spi.relation.VariableReferenceExpression;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;

import javax.inject.Inject;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.facebook.presto.elasticsearch.ElasticsearchAggregationColumnNode.AggregationFunctionColumnNode;
import static com.facebook.presto.elasticsearch.ElasticsearchAggregationColumnNode.GroupByColumnNode;
import static com.facebook.presto.elasticsearch.ElasticsearchErrorCode.DRUID_PUSHDOWN_UNSUPPORTED_EXPRESSION;
import static com.facebook.presto.elasticsearch.ElasticsearchPushdownUtils.computeAggregationNodes;
import static com.facebook.presto.elasticsearch.ElasticsearchQueryGeneratorContext.Origin.DERIVED;
import static com.facebook.presto.elasticsearch.ElasticsearchQueryGeneratorContext.Origin.TABLE_COLUMN;
import static com.google.common.base.MoreObjects.toStringHelper;
import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
import static java.util.Locale.ENGLISH;
import static java.util.Objects.requireNonNull;

public class ElasticsearchQueryGenerator
{
    private static final Logger log = Logger.get(ElasticsearchQueryGenerator.class);
    private static final Map<String, String> UNARY_AGGREGATION_MAP = ImmutableMap.of(
            "min", "min",
            "max", "max",
            "avg", "avg",
            "sum", "sum",
            "distinctcount", "DISTINCTCOUNT");

    private final TypeManager typeManager;
    private final FunctionMetadataManager functionMetadataManager;
    private final StandardFunctionResolution standardFunctionResolution;
    private final ElasticsearchProjectExpressionConverter elasticsearchProjectExpressionConverter;

    @Inject
    public ElasticsearchQueryGenerator(
            TypeManager typeManager,
            FunctionMetadataManager functionMetadataManager,
            StandardFunctionResolution standardFunctionResolution)
    {
        this.typeManager = requireNonNull(typeManager, "type manager is null");
        this.functionMetadataManager = requireNonNull(functionMetadataManager, "function metadata manager is null");
        this.standardFunctionResolution = requireNonNull(standardFunctionResolution, "standardFunctionResolution is null");
        this.elasticsearchProjectExpressionConverter = new ElasticsearchProjectExpressionConverter(typeManager, standardFunctionResolution);
    }

    public static class ElasticsearchQueryGeneratorResult
    {
        private final GeneratedDql generateddql;
        private final ElasticsearchQueryGeneratorContext context;

        public ElasticsearchQueryGeneratorResult(
                GeneratedDql generateddql,
                ElasticsearchQueryGeneratorContext context)
        {
            this.generateddql = requireNonNull(generateddql, "generateddql is null");
            this.context = requireNonNull(context, "context is null");
        }

        public GeneratedDql getGeneratedDql()
        {
            return generateddql;
        }

        public ElasticsearchQueryGeneratorContext getContext()
        {
            return context;
        }
    }

    public Optional<ElasticsearchQueryGeneratorResult> generate(PlanNode plan, ConnectorSession session)
    {
        try {
            ElasticsearchQueryGeneratorContext context = requireNonNull(plan.accept(
                    new ElasticsearchQueryPlanVisitor(session),
                    new ElasticsearchQueryGeneratorContext()),
                    "Resulting context is null");
            return Optional.of(new ElasticsearchQueryGeneratorResult(context.toQuery(), context));
        }
        catch (PrestoException e) {
            log.debug(e, "Possibly benign error when pushing plan into scan node %s", plan);
            return Optional.empty();
        }
    }

    public static class GeneratedDql
    {
        final String table;
        final String dql;
        final boolean pushdown;

        @JsonCreator
        public GeneratedDql(
                @JsonProperty("table") String table,
                @JsonProperty("dql") String dql,
                @JsonProperty("pushdown") boolean pushdown)
        {
            this.table = table;
            this.dql = dql;
            this.pushdown = pushdown;
        }

        @JsonProperty("dql")
        public String getDql()
        {
            return dql;
        }

        @JsonProperty("table")
        public String getTable()
        {
            return table;
        }

        @JsonProperty("pushdown")
        public boolean getPushdown()
        {
            return pushdown;
        }

        @Override
        public String toString()
        {
            return toStringHelper(this)
                    .add("dql", dql)
                    .add("table", table)
                    .add("pushdown", pushdown)
                    .toString();
        }
    }

    private class ElasticsearchQueryPlanVisitor
            extends PlanVisitor<ElasticsearchQueryGeneratorContext, ElasticsearchQueryGeneratorContext>
    {
        private final ConnectorSession session;

        protected ElasticsearchQueryPlanVisitor(ConnectorSession session)
        {
            this.session = session;
        }

        @Override
        public ElasticsearchQueryGeneratorContext visitPlan(PlanNode node, ElasticsearchQueryGeneratorContext context)
        {
            throw new PrestoException(DRUID_PUSHDOWN_UNSUPPORTED_EXPRESSION, "Unsupported pushdown for Elasticsearch connector with plan node of type " + node);
        }

        protected VariableReferenceExpression getVariableReference(RowExpression expression)
        {
            if (expression instanceof VariableReferenceExpression) {
                return ((VariableReferenceExpression) expression);
            }
            throw new PrestoException(DRUID_PUSHDOWN_UNSUPPORTED_EXPRESSION, "Unsupported pushdown for Elasticsearch connector. Expect variable reference, but get: " + expression);
        }

        @Override
        public ElasticsearchQueryGeneratorContext visitMarkDistinct(MarkDistinctNode node, ElasticsearchQueryGeneratorContext context)
        {
            requireNonNull(context, "context is null");
            return node.getSource().accept(this, context);
        }

        @Override
        public ElasticsearchQueryGeneratorContext visitFilter(FilterNode node, ElasticsearchQueryGeneratorContext context)
        {
            context = node.getSource().accept(this, context);
            requireNonNull(context, "context is null");
            Map<VariableReferenceExpression, Selection> selections = context.getSelections();
            ElasticsearchFilterExpressionConverter elasticsearchFilterExpressionConverter = new ElasticsearchFilterExpressionConverter(typeManager, functionMetadataManager, standardFunctionResolution, session);
            String filter = node.getPredicate().accept(elasticsearchFilterExpressionConverter, selections::get).getDefinition();
            return context.withFilter(filter).withOutputColumns(node.getOutputVariables());
        }

        @Override
        public ElasticsearchQueryGeneratorContext visitProject(ProjectNode node, ElasticsearchQueryGeneratorContext contextIn)
        {
            ElasticsearchQueryGeneratorContext context = node.getSource().accept(this, contextIn);
            requireNonNull(context, "context is null");
            Map<VariableReferenceExpression, Selection> newSelections = new LinkedHashMap<>();

            node.getOutputVariables().forEach(variable -> {
                RowExpression expression = node.getAssignments().get(variable);
                ElasticsearchProjectExpressionConverter projectExpressionConverter = elasticsearchProjectExpressionConverter;
                if (contextIn.getVariablesInAggregation().contains(variable)) {
                    projectExpressionConverter = new ElasticsearchAggregationProjectConverter(session, typeManager, functionMetadataManager, standardFunctionResolution);
                }
                ElasticsearchExpression elasticsearchExpression = expression.accept(
                        projectExpressionConverter,
                        context.getSelections());
                newSelections.put(
                        variable,
                        new Selection(elasticsearchExpression.getDefinition(), elasticsearchExpression.getOrigin()));
            });
            return context.withProject(newSelections);
        }

        @Override
        public ElasticsearchQueryGeneratorContext visitLimit(LimitNode node, ElasticsearchQueryGeneratorContext context)
        {
            checkArgument(!node.isPartial(), "Elasticsearch query generator cannot handle partial limit");
            context = node.getSource().accept(this, context);
            requireNonNull(context, "context is null");
            return context.withLimit(node.getCount()).withOutputColumns(node.getOutputVariables());
        }

        @Override
        public ElasticsearchQueryGeneratorContext visitTableScan(TableScanNode node, ElasticsearchQueryGeneratorContext contextIn)
        {
            ElasticsearchTableHandle tableHandle = (ElasticsearchTableHandle) node.getTable().getConnectorHandle();
            checkArgument(!tableHandle.getDql().isPresent(), "Elasticsearch tableHandle should not have dql before pushdown");
            Map<VariableReferenceExpression, Selection> selections = new LinkedHashMap<>();
            node.getOutputVariables().forEach(outputColumn -> {
                ElasticsearchColumnHandle elasticsearchColumn = (ElasticsearchColumnHandle) (node.getAssignments().get(outputColumn));
                checkArgument(elasticsearchColumn.getType().equals(ElasticsearchColumnHandle.ElasticsearchColumnType.REGULAR), "Unexpected elasticsearch column handle that is not regular: " + elasticsearchColumn);
                selections.put(outputColumn, new Selection(elasticsearchColumn.getColumnName(), TABLE_COLUMN));
            });
            return new ElasticsearchQueryGeneratorContext(selections, tableHandle.getTableName(), node.getId());
        }

        @Override
        public ElasticsearchQueryGeneratorContext visitAggregation(AggregationNode node, ElasticsearchQueryGeneratorContext contextIn)
        {
            List<ElasticsearchAggregationColumnNode> aggregationColumnNodes = computeAggregationNodes(node);

            // Make two passes over the aggregationColumnNodes: In the first pass identify all the variables that will be used
            // Then pass that context to the source
            // And finally, in the second pass actually generate the DQL

            // 1st pass
            Set<VariableReferenceExpression> variablesInAggregation = new HashSet<>();
            for (ElasticsearchAggregationColumnNode expression : aggregationColumnNodes) {
                switch (expression.getExpressionType()) {
                    case GROUP_BY: {
                        GroupByColumnNode groupByColumn = (GroupByColumnNode) expression;
                        VariableReferenceExpression groupByInputColumn = getVariableReference(groupByColumn.getInputColumn());
                        variablesInAggregation.add(groupByInputColumn);
                        break;
                    }
                    case AGGREGATE: {
                        AggregationFunctionColumnNode aggregationNode = (AggregationFunctionColumnNode) expression;
                        variablesInAggregation.addAll(
                                aggregationNode.getCallExpression().getArguments().stream()
                                        .filter(argument -> argument instanceof VariableReferenceExpression)
                                        .map(argument -> (VariableReferenceExpression) argument)
                                        .collect(Collectors.toList()));
                        break;
                    }
                    default:
                        throw new PrestoException(DRUID_PUSHDOWN_UNSUPPORTED_EXPRESSION, "Unsupported pushdown for Elasticsearch connector. Unknown aggregation expression:" + expression.getExpressionType());
                }
            }

            // now visit the child project node
            ElasticsearchQueryGeneratorContext context = node.getSource().accept(this, contextIn.withVariablesInAggregation(variablesInAggregation));
            requireNonNull(context, "context is null");
            checkArgument(!node.getStep().isOutputPartial(), "Elasticsearch pushdown does not support partial aggregations");

            // 2nd pass
            Map<VariableReferenceExpression, Selection> newSelections = new LinkedHashMap<>();
            Map<VariableReferenceExpression, Selection> groupByColumns = new LinkedHashMap<>();
            Set<VariableReferenceExpression> hiddenColumnSet = new HashSet<>(context.getHiddenColumnSet());
            int aggregations = 0;
            boolean groupByExists = false;

            for (ElasticsearchAggregationColumnNode expression : aggregationColumnNodes) {
                switch (expression.getExpressionType()) {
                    case GROUP_BY: {
                        GroupByColumnNode groupByColumn = (GroupByColumnNode) expression;
                        VariableReferenceExpression groupByInputColumn = getVariableReference(groupByColumn.getInputColumn());
                        VariableReferenceExpression outputColumn = getVariableReference(groupByColumn.getOutputColumn());
                        Selection elasticsearchColumn = requireNonNull(context.getSelections().get(groupByInputColumn), "Group By column " + groupByInputColumn + " doesn't exist in input " + context.getSelections());

                        newSelections.put(outputColumn, new Selection(elasticsearchColumn.getDefinition(), elasticsearchColumn.getOrigin()));
                        groupByColumns.put(outputColumn, new Selection(elasticsearchColumn.getDefinition(), elasticsearchColumn.getOrigin()));
                        groupByExists = true;
                        break;
                    }
                    case AGGREGATE: {
                        AggregationFunctionColumnNode aggregationNode = (AggregationFunctionColumnNode) expression;
                        String elasticsearchAggregationFunction = handleAggregationFunction(aggregationNode.getCallExpression(), context.getSelections());
                        newSelections.put(getVariableReference(aggregationNode.getOutputColumn()), new Selection(elasticsearchAggregationFunction, DERIVED));
                        aggregations++;
                        break;
                    }
                    default:
                        throw new PrestoException(DRUID_PUSHDOWN_UNSUPPORTED_EXPRESSION, "Unsupported pushdown for Elasticsearch connector. Unknown aggregation expression:" + expression.getExpressionType());
                }
            }

            // Handling non-aggregated group by
            if (groupByExists && aggregations == 0) {
                VariableReferenceExpression hidden = new VariableReferenceExpression(UUID.randomUUID().toString(), BigintType.BIGINT);
                newSelections.put(hidden, new Selection("count(*)", DERIVED));
                hiddenColumnSet.add(hidden);
                aggregations++;
            }
            return context.withAggregation(newSelections, groupByColumns, aggregations, hiddenColumnSet);
        }

        private String handleAggregationFunction(CallExpression aggregation, Map<VariableReferenceExpression, Selection> inputSelections)
        {
            String prestoAggregation = aggregation.getDisplayName().toLowerCase(ENGLISH);
            List<RowExpression> parameters = aggregation.getArguments();
            if (prestoAggregation.equals("count")) {
                if (parameters.size() <= 1) {
                    return format("count(%s)", parameters.isEmpty() ? "*" : inputSelections.get(getVariableReference(parameters.get(0))));
                }
            }
            else if (UNARY_AGGREGATION_MAP.containsKey(prestoAggregation) && aggregation.getArguments().size() == 1) {
                return format("%s(%s)", UNARY_AGGREGATION_MAP.get(prestoAggregation), inputSelections.get(getVariableReference(parameters.get(0))));
            }
            throw new PrestoException(DRUID_PUSHDOWN_UNSUPPORTED_EXPRESSION, "Unsupported pushdown for Elasticsearch connector. Aggregation function: " + aggregation + " not supported");
        }
    }
}
