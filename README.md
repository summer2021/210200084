# Openlookeng-elasticsearchPushDown

## 1.项目描述

**以下是 算子下推开发说明简介**
当前通过openLooKeng Elasticsearch Connector查询Elasticsearch数据源数据时，需要全数据表取回。当涉及的数据量比较大，且 openLooKeng 中输入的 SQL 语句含有较多的数据过滤条件，数据项全取回时大量的取回的数据为无用数据，非常浪费网络带宽。Elasticsearch数据源具有较强的计算能力，能够对数据进行过滤计算操作。将openLooKeng内核中的过滤算子映射为 Elasticsearch 可执行的过滤操作，使得数据能够提前在 Elasticsearch过滤计算 （例如将 openLooKeng 中的‘count’算子推到 Elasticsearch），减少数据传输，使得 openLooKeng SQL 作业执行时间大幅缩短，是本项目的需要达到的目标。

## 2.编写目标

本文用于介绍Openlookeng-Elasticsearch的算子下推设计，Openlookeng源码版本为v1.2.0。本文的目的在于***学习和复用原有下推框架来提供一种基于openLooKeng和Elasticsearch的大数据***查询优化方法，其使用openLooKeng集群进行SQL的接收和解析，利用Elasticsearch快速查询和过滤的特性，在Elasticsearch集群中预先过滤部分数据，减少多余数据的读取，实现数据的算子下推，从而提高查询效率。 

## 3.软件架构

基于jdbc的connector（eg. Mysql /oracle/hana/DC）都为其增加了Connectorplanoptimizer，直接继承Basejdbc 里面的planoptimizer，如果jdbc的数据源在语法上有特殊之处，只需要覆盖写几个类就行了，即将语法不同的几个函数limit/filter... 等覆写。

2.  ElasticsearchConnector中复写下面的函数：getconnectorPlanOptimizerProvider()
2.  ElasticsearchPlanOptimizerProvider的实现
3.  PlanOptimizer里面的optimize函数
4.  ElasticsearchQueryGenerator的实现
5.  ElasticsearchPushDowenUtils
6.  ElasticsearchStatementwriter
7.  ElasticsearchProjectExpressionConverter

## 4.说明文档

### 1.openLooKeng执行计划优化流程

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps1.jpg) 

 

图4-3 OpenLooKeng/Presto执行计划优化流程

如上图所示，接收到用户SQL语句之后，SQL被转换成一个Abstract syntax tree (AST)树。AST树再被转换成逻辑执行计划树。然后，也就是执行计划优化过程中最重要的一步，使用规则（Rule）或者优化器（Optimizers）进行执行计划优化，每一个计划优化器（PlanOptimizer）都可以对整个计划树的子树进行操作，并用一个更优的子执行计划（如基于启发式或统计数据的更优化的子树）替换当前的子树，从而达到优化的目的。

PlanOptimizers通常是长期的经验累积得出来的一些优化规则，比如谓词下推、join reorder等等。PlanOptimizers可以存储一些物理执行信息在handles (如ConnectorTableHandle,ConnectorTableLayoutHandle,ConnectorPartitionHandle, …)connector句柄中。新下推框架则工作在这一层。

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps2.jpg) 

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps3.jpg) 

与其他一些SQL引擎不同，Presto没有明确设置逻辑计划和物理计划之间的边界。相反，有一些关键的优化器（如AddExchanges）可以将逻辑计划转换为物理计划。AddExchanges将数据混洗（数据交换）运算符添加到查询执行中。这一重要步骤决定了查询执行如何并行化，以及如何重新分配数据以在查询的每个阶段进行处理。Presto 中的一个执行阶段通常是对分区键上的数据进行混洗，这是处理查询计划的下一部分所需的。AddExchanges依赖于从connector（connector）返回的Handle来决定要添加到计划中的合适位置和交换器类型。得到最优的执行计划之后，逻辑执行计划被转换成物理执行计划，然后被分片，分割成按照stage执行的一个个子树，最终调度到worker上执行。

PickTableLayout通过调用connector提供的 API 方法，计划谓词下推到表扫描getTableLayout中。它还用于从connector获取物理布局信息。getTableLayout返回LayoutHandleconnector填充的有关扫描将返回的数据结构的信息。Presto 稍后将使用LayoutHandle来计划、优化和执行查询。依靠PickTableLayout同时进行谓词下推和物理规划是非常受限的，因为connector无法在基本谓词下推之外修改计划。

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps4.jpg) 

### 2. **prestoSql的执行过程局限性**

根据社区的讨论，PrestoSql原下推框架有一些目标:

（1）使用现有的Rule或Optimizer的框架，而且不是使用基于visitor模式的PlanOptimizers来实现下推，同时能够让connector提供转换rules来实现下推；

（2）并不是建立一个原生的机制来支持所有操作的下推。

prestoSql的执行过程：

（1）首先，引入一些列的下推规则，每一个规则负责下推相应的操作到TableScan操作中，比如PushFilterIntoConnector, PushProjectionIntoConnector, PushAggregationIntoConnector等等；

（2）上述的这些rules通过指定的metadata调用与connectors交互，如果connector支持这个操作下推，操作则被下推到TableScan操作，同时在connectorTableHadle中记录相关信息。

 

Presto定义了一个connector API，它允许Presto查询具有connector实现的任何数据源。现有的connector API提供了基本的谓词下推功能，允许connector在底层数据源上执行过滤。但是，现有的谓词下推功能存在一些限制，这限制了connector可以做什么。可以向下推的表达性是有限的，connector根本不能改变计划的结构。下图显示了执行计划和 connector 以往交互的方式：

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps5.jpg) 

首先，Presto只支持通过connector提供的方法进行谓词向下推。如果 Presto需要按下一组操作(例如，projection/ filter/ aggregation/ limit），那么connector需要支持几种方法：pushDownFilterLimitScan/pushDownProjectFilterScan…，这增加了创建和维护connector的复杂性。此外，我们不仅要下推操作，还要将新操作添加到查询计划中。当前的计划器模型确实支持许多有用的connector驱动的计划更改。

其次，可以下推的谓词和运算符的范围是有限的。只有可以由称为 ***\**TupleDomain\**\*** 的数据结构表示的谓词才能被下推。此数据结构仅支持 AND 谓词，用于确定变量/列是否在值集（范围或公平）中。因此没有办法描述像'A[1] IN (1, 2, 3)'或‘A like 'A Varchar %'’之类的复杂谓词。

更灵活的方法是将当前表示为抽象语法树 (AST) 的整个表达式下推。这种方法的一个问题是当添加新的语言功能时，AST会随着时间的推移而发展。此外，AST不包含类型信息以及执行函数解析的足够信息。

目前，社区还开发了另一个特性，称为动态函数注册。由于添加了动态函数注册（dynamic function registration），函数现在可以解析为不同的实现。动态函数注册允许用户编写自己的 SQL 函数。例如，用户可以在使用该函数的查询仍在运行时更新另一个会话中 SQL 函数的定义。如果我们要在调用时执行函数解析，那么我们最终可能会在同一会话和查询中使用不同的实现。如果我们要支持物化视图，我们还需要确保数据读取器和写入器之间的函数版本一致。通过将函数解析信息存储在表达式本身中作为可序列化的functionHandle来解决这个问题。当我们重用包含该函数的表达式时可以一致地引用该函数。

类型（Types）存在类似的问题。connector不能安全地依赖元数据来了解变量的类型。描述变量的元数据可能不可用或在执行期间已更改。Presto 0.217 和 0.229 版本之间逐步提高了 Presto规划器下推的能力，使connectors能够理解和操作计划子树。

### 3. **OpenLookeng**/Presto下推接口

将SQL进行解析，对应生成执行数解析。这个过程就会对执行的谓词算子进行优化，将过滤的计划节点提前下推。

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps6.jpg) 

对应实现计划树的优化接口，现有实现下推接口有Limit，Predicate，SubQuery，WindowFilter。

### 4. 适配新的下推框架方案与实现

openLooKeng新下推框架的主要思想是把执行计划子树暴露给connector，让connector提供PlanOptimizers（基于visitor模式的）给执行优化引擎，这样可以让connector引入任意的优化。

为了防止一个connector的PlanOptimizers修改其他connector的执行计划子树，openLooKeng对于暴露给Connector的PlanNode做了两个限制：

1）暴露出来的PlanNodes须移动到presto-spi模块；

2）仅仅暴露属于connector的子执行计划树给相应的connector。

满足上述规则的子最大树将被转换为由connector提供的优化规则选择的更优化的形式：

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps7.jpg) 

注意上述规则仅适用于maxSubPlan作为输入的优化器。一个connector提供的优化器很有可能生成一个包含属于另一个connector节点的新计划（通常在视图扩展期间）。在这种情况下，TableScan可能从结合了来自多个不同数据源的数据虚拟表（virtual table）中读取数据。该虚拟表的TableScan可以扩展为一个新的子树，将两个connector的TableScan进行Union。一旦扩展，新生成的计划节点的优化就可以由它们所属的connector处理，从而可以实现最优子计划。

 

 

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps8.jpg) 

connector规则将转换子最大计划树。引擎将在关键计划转换检查点（critical plan transformation checkpoints）应用connector优化规则：在逻辑计划上运行的所有规则将在AddExchange开始转换为物理计划之前应用一次。此时，我们可以展开视图，下推很多操作。

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps9.jpg) 

一些依赖物理信息的优化需要在优化周期结束之后应用。例如，我们可能只想将聚合的一部分下推到connector中，以便仍能从并行执行中获益。在添加交换节点之后，将会拆分聚合阶阶段。

在谓词下推的情况下（以MySQL Connector实例为例），connector可以将MySQL可以处理的谓词作为SQL表达式保存在MySQLConnectorLayoutHandle里面，并返回一个TableScan节点。如下图所示，左子树只会暴露给Hive Connector，右子树只会暴露给Mysql Connector。然后会应用他们各自的PlanOptimizers。

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps10.png) 

 

 

openLooKeng的下推框架如下图所示：

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps11.jpg)新下推框架的工作原理很简单，主要分为两步：

1）Connector在启动的时候会告诉执行优化引擎其提供的ConnectorPlanOptimizer，如下图的HiveFpuPushdownOptimizer，其需要实现上图的optimize接口，optimize函数以子执行计划为入口，返回优化后的执行计划；

2）在执行优化引擎中引入ApplyConnectorOptimization优化器，

该Optimizer会把根据子执行计划所在的connector，调用其connectorPlanOptimizer。如下图所示，经过HiveFpuPushdownOptimizer优化之后，Aggregation和Filter操作都下推到了数据源中。

 

具体实现如下：

第一：在ElasticsearchConnector中复写下面的函数

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps12.jpg) 

第二：实现ElasticsearchPlanOptimizer 接口如下：

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps13.jpg) 

第三：实现ElasticsearchPlanOptimizerProvider

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps14.jpg) 

第四：实现PlanOptimizer里面的optimize函数，主要是实现一个visitor去visit执行计划树

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps15.jpg) 

第五：实现Visitor，用来生成下推的语句，同时修改执行计划树

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps16.jpg) 

第六：实现ElasticsearchQueryGenerator，在ElasticsearchQueryGenerator中实现一个visitor用来把下推的信息记录到ElasticsearchQueryGeneratorContext，如果存在节点可以下推，则生成对应的sql：

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps17.jpg) 

### 5. **JDBC** **连接器计划优化器（Connectorplanoptimizer）**

基于jdbc的connector（如Mysql /oracle/hana/DC）都为其增加了Connectorplanoptimizer，直接继承Basejdbc 里面的planoptimizer，如果jdbc的数据源在语法上有特殊之处，只需要覆盖写几个类就行了，即将语法不同的几个函数limit/filter... 等进行覆写。

Openlookeng还实现了JdbcPlans优化器。JdbcPlant优化器将PlanNode树的子树作为输入，尝试将其转换为优化的形式，并生成发送到JDBC数据源的相应SQL。 BaseJdbcQuery生成器用于访问子树并保存JdbcQuery生成器上下文信息。 采用自下而上的方法访问PlanNode子树。结果将保存在JdbcQuery生成器结 果中。最后，BaseJdbcQuery生成器将调用构建SQl来生成发送到数据源的 SQL。 还有两个重要的接口，即行表达式转换器和SQL状态写器。行表达式转换器用于操作RowExpression.SqlStatementWriter用于生成SQL语句。对于不同的Jdbc数据 源，它们可能有不同的系统税。例如，Oracle使用关键字ROWNUM作为限制条款，但Mysql和Postgresql对限制子句使用关键字限制。 BaseJdbcSqlStatementWriter是Sql状态编写器的默认实现。如果数据源具有不同的语法，则开发人员可以覆盖相应的函数。下推的设计是基于PrestoDB的下推解决方案。

 

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps18.jpg) 

 

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps19.jpg) 

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml22840\wps20.jpg) 

### 6. **总结**

通过上述的介绍，以上详细说明了Presto-Connector新旧下推框架的实现方法的实现流程特点总结及拓展Connector新的算子下推的开发步骤。

#### 参与贡献

1.  Fork 本仓库
2.  新建 elasticsearchPushDown 分支
3.  提交代码
4.  新建 Pull Request

